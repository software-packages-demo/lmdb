# lmdb

Lightning Memory-Mapped Database. https://symas.com/lmdb

# Unofficial documentation
* [*Lightning Memory-Mapped Database*
  ](https://en.wikipedia.org/wiki/Lightning_Memory-Mapped_Database)
  (Wikipedia)

# Searching for lmdb language bindings
* [repology](https://repology.org/projects/?search=lmdb)
* go
* haskell
* perl
* perl6 - raku
* PHP: [*List of DBA handlers*](https://www.php.net/manual/en/dba.requirements.php)
* python
* rust
* tcl
* C++ [lmdb++](https://repology.org/project/lmdb%2B%2B/versions)

# Software using lmdb
* [LMDB/memcachedb](https://github.com/LMDB/memcachedb)
  (2015) memcachedb ported from BerkeleyDB to LMDB originally from
  http://memcachedb.googlecode.com/svn/trunk
  http://lmdb.tech/bench/memcache